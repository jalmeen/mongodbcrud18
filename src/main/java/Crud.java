import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.util.Iterator;

public class Crud extends Connection {
    private MongoDatabase db;
    private MongoCollection<Document> collectionName;

    public void dbs() {
        db = mongoClient.getDatabase("test2");
        System.out.println("db test is in use");
    }

    public void createCollection(){
        collectionName = db.getCollection("Patient");

    }

    public void createDocument() {

        Document document = new Document("patient_name", "Chelsey")
                .append("patient_adress", "12-B Suryavillas,Gurgaon")
                .append("gender", "Female")
                .append("age", "34")
                .append("patient_code", "125890")
                .append("allergies", "Wheezing");
        collectionName.insertOne(document);
        System.out.println("Document inserted successfully");
    }

    public void findDocumentInserted(){
        FindIterable<Document> iterDoc = collectionName.find();
        int i = 1;

        // Getting the iterator
        Iterator it = iterDoc.iterator();

        while (it.hasNext()) {
            System.out.println(it.next());
            i++;
        }
        System.out.println("Reading documents ");
    }

    public void updateDocument(){
        collectionName.updateOne(Filters.eq("patient_name", "Chelsey"), Updates.set("age", 36));
        System.out.println("Document updated");
    }

    public void deleteDocument(){
        collectionName.deleteOne(Filters.eq("patient_code", 125890));
        System.out.println("Document deleted");
    }
}
